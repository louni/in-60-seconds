@snap[midpoint span-90 text-25]
#### Publication d'un site statique en CI/CD sur AWS
@snapend

@snap[north-west span-15]
![](assets/img/Sogeti-logo-2018.png)
@snapend

@snap[north-east span-15]
![](assets/img/AJC.png)
@snapend

@snap[south text-06 text-gray span-100]
Projet fin de formation DevOps AWS
@snapend

---

## Trombinoscope

|||||||
|:---:|:---:|:---:|:---:|:---:|:---:|
|![Photo](assets/img/img.png "Ben Salah Raouia")|![Photo](assets/img/img.png "Bresson Kevin")|![Photo](assets/img/img.png "Dote Lina")|![Photo](assets/img/img.png "Lassissi Mohamed")|

|||||||
|:---:|:---:|:---:|:---:|:---:|:---:|
|![Photo](assets/img/img.png "Louni Amar")|![Photo](assets/img/img.png "Pasteur Emilie")|![Photo](assets/img/img.png "Ramos Pascal")|![Photo](assets/img/img.png "Sene Assane")|

||||||
|:---:|:---:|:---:|:---:|:---:|
|![Photo](/path/img.png "Tavernel Sarah")|![Photo](/path/img.png "Tebachi Sarah")|![Photo](/path/img.png "Valere Stephanie")|

---
---
@title[Customize Slide Layout]

@snap[west span-50]
## Customize Slide Content Layout
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

---?color=#E58537
@title[Add A Little Imagination]

@snap[north-west]
#### Add a splash of @color[cyan](**color**) and you are ready to start presenting...
@snapend

@snap[west span-55]
@ul[spaced text-white]
- You will be amazed
- What you can achieve
- *With a little imagination...*
- And **GitPitch Markdown**
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/conference.png)
@snapend

---?image=assets/img/presenter.jpg

@snap[north span-100 headline]
## Now It's Your Turn
@snapend

@snap[south span-100 text-06]
[Click here to jump straight into the interactive feature guides in the GitPitch Docs @fa[external-link]](https://gitpitch.com/docs/getting-started/tutorial/)
@snapend
